package com.smartwatch.mumu13ms.smartwatch;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Menu extends AppCompatActivity {
    ImageButton btnBrowser, btnProyektor, btnBoom, btnMaps, btnSuhu, btnSetting, btnRemote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btnBoom = (ImageButton) findViewById(R.id.btnBoom);
        btnBrowser = (ImageButton) findViewById(R.id.btnBrowser);
        btnMaps = (ImageButton) findViewById(R.id.btnFollow);
        btnProyektor = (ImageButton) findViewById(R.id.btnProjektor);
        btnRemote = (ImageButton) findViewById(R.id.btnRemote);
        btnSuhu = (ImageButton) findViewById(R.id.btnUkur);
        btnSetting = (ImageButton) findViewById(R.id.btnSetting);

        actionInActivitie();

    }

    public void actionInActivitie(){
        btnBoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Boom.class);
                startActivity(i);
            }
        });
        btnBrowser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://www.google.com");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Maps.class);
                startActivity(i);
            }
        });
        btnProyektor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Projektor.class);
                startActivity(i);
            }
        });
        btnRemote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Remote.class);
                startActivity(i);
            }
        });
        btnSuhu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Suhu.class);
                startActivity(i);
            }
        });
        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Setting.class);
                startActivity(i);
            }
        });
    }
}
